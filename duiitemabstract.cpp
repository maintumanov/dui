#include "duiitemabstract.h"

duiItemAbstract::duiItemAbstract(quint8 channel, QWidget *parent) : QWidget(parent)
{
    ichannel = channel;
    frame.setFrameShape(QFrame::StyledPanel);
    frame.setFrameShadow(QFrame::Plain);
    iname = QString(tr("Channel %1")).arg(ichannel);
}

QWidget *duiItemAbstract::getWidget()
{
    return new QWidget();
}

void duiItemAbstract::setName(QString name)
{
    iname = name;
}

void duiItemAbstract::setSuffix(QString suffix)
{
    isuffix = suffix;
}

quint8 duiItemAbstract::getChannel()
{
    return ichannel;
}

quint8 duiItemAbstract::getType()
{
    return 0;
}

QString duiItemAbstract::getName()
{
    return iname;
}

QString duiItemAbstract::getSuffix()
{
    return isuffix;
}

void duiItemAbstract::reciveData(QByteArray )
{

}
