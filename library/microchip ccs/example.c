#include <example.h>
#include "dui.c"

//TASK MESSAGE CHECK
#task(rate = 100 ms, max = 30 us)
void task_check();

#task(rate = 1 ms, max = 300 us)
void task_DUIcheck();

//-------------------------------------------------
void task_check() {
   output_high(LED1);
   int8 t = 0;
   output_low(PWM);

   output_float(PWM);
   while (input_state(PWM) == 0 && t < 200) t++;
   DUISendInt8(0, t);
   output_low(PWM);
   output_low(LED1);
}

void task_DUIcheck() {
   DUICheck();
}

void formInit(void) {
   char text1[]="Capacity";
   char text2[]="mm";
   DUIAddChart(0, text1, text2);
 //  DUIAddEditUint8(0,1,2,3,text1, text2);
 //  output_high(LED2);
}

//implementation
void initialization() {
    SET_TRIS_A( 0x00 );
    SET_TRIS_B( 0x01 );
    SET_TRIS_C( 0x84 );
    DUIInit();
    DUIOnFormInit = formInit;
}
// main cicle program
void main()
{
   initialization();
   rtos_run();
}
