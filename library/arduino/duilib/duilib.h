/*
 DUILib.h - Library for Arduino 
 Author: Tumanov Stanislav Aleksandrovich
 Created: 25/08/2016
 For further information see:
*/

#ifndef DUILIB_h
#define DUILIB_h

#include <Arduino.h>

#ifndef QueueSize
#define QueueSize 8
#endif

#ifndef Queue_SIZE
#define Queue_SIZE 16
#endif
#define Queue_MASK    (Queue_SIZE - 1)

typedef void (*DUIPOn)(byte, byte (*));
typedef void (*DUIPInit)(void);

typedef struct
{
  byte channel;
  byte dlc;                // Number of data bytes
  byte data[128];            // Data bytes
  byte analysisState;
  byte dps;
} snFrame;

class DUILib
{
  public:
    DUIPOn OnReadData;
    DUIPInit OnFormInit;
    
    void Init();
    void Check();
	
    void SendStateTrueFalse(byte channel, bool vol); //tested
    void SendStateOnOff(byte channel, bool vol); 
    void SendStateYesNo(byte channel, bool vol); 
    void SendStateOpenClose(byte channel, bool vol); 
    void SendUInt8(byte channel, byte vol); //tested
    void SendInt8(byte channel, char vol);
    void SendUInt16(byte channel, word vol);
    void SendInt16(byte channel, int vol);
    void SendUInt32(byte channel, unsigned long vol);
    void SendInt32(byte channel, long vol);
    void SendString(byte channel, String text); //tested
  //  void SendLog(String text); //tested
    void SendEnableButton(byte channel, byte numButon, bool enabled); 
    void SendTemperature(byte channel, byte H, byte L); //tested

    String ReadString(byte addr, const byte *data); //tested
    word ReadUInt16(byte addr, const byte *data);
    int ReadInt16(byte addr, const byte *data);
    unsigned long ReadUInt32(byte addr, const byte *data);
    long ReadInt32(byte addr, const byte *data);
    
    void addLabel(byte channel, String cname, String suffix); //tested
    void addLed(byte channel, byte colorIndex, String cname); //tested
    void addChart(byte channel, String cname, String suffix); //tested
    void addLog(byte channel, byte lines, String cname);
	//=========================================
    void addEditUint8(byte channel, byte vol, byte min, byte max, String cname, String suffix); //tested
    void addEditInt8(byte channel, char vol, char min, char max, String cname, String suffix); 
    void addEditUInt16(byte channel, word vol, word min, word max, String cname, String suffix);
    void addEditInt16(byte channel, int vol, int min, int max, String cname, String suffix); 
    void addEditInt32(byte channel, long vol, long min, long max, String cname, String suffix); 
	//=========================================
    void addSliderUint8(byte channel, byte vol, byte min, byte max, String cname, String suffix); //tested
    void addSliderInt8(byte channel, char vol, char min, char max, String cname, String suffix); 
    void addSliderUInt16(byte channel, word vol, word min, word max, String cname, String suffix);
    void addSliderInt16(byte channel, int vol, int min, int max, String cname, String suffix); 
    void addSliderInt32(byte channel, long vol, long min, long max, String cname, String suffix); 
	//=========================================
    void addCheckBox(byte channel, bool vol, String cname); //tested
    void addTextEdit(byte channel, String text, String cname); //tested
    void addButton(byte channel, String cname); //tested
    void addTwoButtons(byte channel, String b1name, String b2name); 
    void addThreeButtons(byte channel, String b1name, String b2name, String b3name); 
    void addEditTemperature(byte channel, byte H, byte L, String cname); 
	//=========================================
  private:
	bool TransmitEnable = 0;
    snFrame rx_message;
    unsigned int make16(byte a, byte b);
    void SendStart();
    void SendByte(byte data);
    void SendWord(word data);
	void SendInt(int data);
    void SendULong(unsigned long data);
    void SendLong(long data);
    void SendData(byte dsize, const byte *data);
    void SendString(String str);
    void SendPacket(byte channel, byte dsize, const byte *data);
    void Analysis(byte data);

};

#endif
