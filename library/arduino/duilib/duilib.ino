#include "duilib.h"

DUILib DUI;

const int ledPin =  13;     
int ledState = LOW;  
unsigned long tmp = 0;
bool led13Satet = 0;

unsigned long previousMillis = 0;      
const long interval = 700;         

void OnFormInitialization() {
  DUI.addLed(1, 0, "Состояние");
  DUI.addLabel(0, "UINT32", "");
  DUI.addButton(2, "Led 13");
  DUI.addLog(3, 20, "ЖУРНАЛ");
}

void OnRead(byte channel, byte (*data)) {
  switch (channel) {
    case 2: 
      led13Satet = !led13Satet;
      digitalWrite(ledPin, led13Satet);

      break;   
  }
}

void setup() {
  pinMode(ledPin, OUTPUT);
  DUI.Init();
  DUI.OnFormInit = OnFormInitialization;
  DUI.OnReadData = OnRead;
}

void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;

    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }

    DUI.SendStateTrueFalse(1, ledState);
    DUI.SendUInt32(0, tmp);
    tmp ++;
    DUI.SendString(3, "LOG");
  }
  DUI.Check();
}
