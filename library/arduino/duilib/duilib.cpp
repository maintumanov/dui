/*
 duilib.cpp - Library for 
 Author: Tumanov Stanislav Aleksandrovich
 Created: 25/07/2016
 For further information see:
*/

#include <Arduino.h>
#include <EEPROM.h>

#include "duilib.h"

void DUILib::Init()
{
  Serial.begin(115200);
  TransmitEnable = 0;
  Serial.write(254);
}

void DUILib::Check()
{
  while (Serial.available() > 0) Analysis(Serial.read());
}

void DUILib::SendStateTrueFalse(byte channel, bool vol) {
  if (!TransmitEnable) return;
  SendStart();
  SendByte(channel);
  SendByte(2);
  SendByte(0);
  SendByte(vol);
}

void DUILib::SendStateOnOff(byte channel, bool vol) {
  if (!TransmitEnable) return;
  SendStart();
  SendByte(channel);
  SendByte(2);
  SendByte(1);
  SendByte(vol);
}

void DUILib::SendStateYesNo(byte channel, bool vol) {
  if (!TransmitEnable) return;
  SendStart();
  SendByte(channel);
  SendByte(2);
  SendByte(2);
  SendByte(vol);
}

void DUILib::SendStateOpenClose(byte channel, bool vol) {
  if (!TransmitEnable) return;
  SendStart();
  SendByte(channel);
  SendByte(2);
  SendByte(3);
  SendByte(vol);
}

void DUILib::SendUInt8(byte channel, byte vol)
{
  if (!TransmitEnable) return;
  SendStart();
  SendByte(channel);
  SendByte(2);
  SendByte(4);
  SendByte(vol);
}

void DUILib::SendInt8(byte channel, char vol)
{ 
  if (!TransmitEnable) return;
  SendStart();
  SendByte(channel);
  SendByte(2);
  SendByte(5);
  SendByte(vol);
}

void DUILib::SendUInt16(byte channel, word vol)
{
  if (!TransmitEnable) return;
  SendStart();
  SendByte(channel);
  SendByte(3);
  SendByte(6);
  SendWord(vol);
}

void DUILib::SendInt16(byte channel, int vol) 
{
  if (!TransmitEnable) return;
  typedef volatile union {
    int i;
    word w;
  } t_c;
  t_c c;
  c.i = vol;
  SendStart();
  SendByte(channel);
  SendByte(3);
  SendByte(7);
  SendWord(c.w);
}

void DUILib::SendUInt32(byte channel, unsigned long vol) {
  if (!TransmitEnable) return;
  SendStart();
  SendByte(channel);
  SendByte(5);
  SendByte(9);
  SendULong(vol);
}

void DUILib::SendInt32(byte channel, long vol) {
  if (!TransmitEnable) return;
  SendStart();
  SendByte(channel);
  SendByte(5);
  SendByte(8);
  SendLong(vol);
}

void DUILib::SendString(byte channel, String text) {
  if (!TransmitEnable) return;
  SendStart();
  SendByte(channel);
  SendByte(text.length() + 2);
  SendByte(13);
  SendString(text);
}

// void DUILib::SendLog(String text) {
//   SendString(254, text);
// }

void DUILib::SendEnableButton(byte channel, byte numButon, bool enabled) {
  if (!TransmitEnable) return;
  byte vol = numButon;
  if (enabled) vol = vol + 3;
  SendStart();
  SendByte(channel);
  SendByte(2);
  SendByte(4);
  SendByte(vol);
}

void DUILib::SendTemperature(byte channel, byte H, byte L) {
  if (!TransmitEnable) return;
  SendStart();
  SendByte(channel);
  SendByte(3);
  SendByte(10);
  SendByte(H);  
  SendByte(L);  
}

//================================
String DUILib::ReadString(byte addr, const byte *data) {
  byte lenght = data[addr];
  char str[lenght + 1];
  for (int i = 0; i < lenght; i ++) str[i] = data[i + addr + 1];
  str[lenght] = 0;
  String strr(str);
  return strr;
}

word ReadUInt16(byte addr, const byte *data) {
  typedef volatile union {
    word i;
    byte w[2];
  } t_c;
  t_c c;
  c.w[0] = data[addr];
  c.w[1] = data[addr + 1]; 
  return c.i;
}

int ReadInt16(byte addr, const byte *data) {
  typedef volatile union {
    int i;
    byte w[2];
  } t_c;
  t_c c;
  c.w[0] = data[addr + 1];
  c.w[1] = data[addr]; 
  return c.i;
} 

unsigned long ReadUInt32(byte addr, const byte *data) {
  typedef volatile union {
    unsigned long i;
    byte w[4];
  } t_c;
  t_c c;
  c.w[0] = data[addr + 3];
  c.w[1] = data[addr + 2]; 
  c.w[2] = data[addr + 1]; 
  c.w[3] = data[addr]; 
  return c.i;  
}

long ReadInt32(byte addr, const byte *data) {
  typedef volatile union {
    long i;
    byte w[4];
  } t_c;
  t_c c;
  c.w[0] = data[addr + 3];
  c.w[1] = data[addr + 2]; 
  c.w[2] = data[addr + 1]; 
  c.w[3] = data[addr]; 
  return c.i;  
}
//=========================================

void DUILib::addLabel(byte channel, String cname, String suffix) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(1);
  SendByte(cname.length() + suffix.length() + 2);
  SendString(cname);
  SendString(suffix);
}

void DUILib::addLed(byte channel, byte colorIndex, String cname) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(2);
  SendByte(cname.length() + 2);
  SendByte(colorIndex);
  SendString(cname);
}

void DUILib::addChart(byte channel, String cname, String suffix) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(3);
  SendByte(cname.length() + suffix.length() + 2);
  SendString(cname);
  SendString(suffix);
}

void DUILib::addLog(byte channel, byte lines, String cname) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(10);
  SendByte(cname.length() + 2);
  SendByte(lines);
  SendString(cname);
}
//=========================================

void DUILib::addEditUint8(byte channel, byte vol, byte min, byte max, String cname, String suffix) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(4);
  SendByte(cname.length() + suffix.length() + 6);
  SendByte(4);
  SendByte(vol);
  SendByte(min);
  SendByte(max);
  SendString(cname);
  SendString(suffix);
}

void DUILib::addEditInt8(byte channel, char vol, char min, char max, String cname, String suffix) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(4);
  SendByte(cname.length() + suffix.length() + 6);
  SendByte(5);
  SendByte(vol);
  SendByte(min);
  SendByte(max);
  SendString(cname);
  SendString(suffix);
}

void DUILib::addEditUInt16(byte channel, word vol, word min, word max, String cname, String suffix) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(4);
  SendByte(cname.length() + suffix.length() + 9);
  SendByte(6);
  SendWord(vol);
  SendWord(min);
  SendWord(max);
  SendString(cname);
  SendString(suffix);
}

void DUILib::addEditInt16(byte channel, int vol, int min, int max, String cname, String suffix) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(4);
  SendByte(cname.length() + suffix.length() + 9);
  SendByte(7);
  SendInt(vol);
  SendInt(min);
  SendInt(max);
  SendString(cname);
  SendString(suffix);
}

void DUILib::addEditInt32(byte channel, long vol, long min, long max, String cname, String suffix) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(4);
  SendByte(cname.length() + suffix.length() + 15);
  SendByte(8);
  SendLong(vol);
  SendLong(min);
  SendLong(max);
  SendString(cname);
  SendString(suffix);
}
//=========================================
void DUILib::addSliderUint8(byte channel, byte vol, byte min, byte max, String cname, String suffix) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(5);
  SendByte(cname.length() + suffix.length() + 6);
  SendByte(4);
  SendByte(vol);
  SendByte(min);
  SendByte(max);
  SendString(cname);
  SendString(suffix);
}

void DUILib::addSliderInt8(byte channel, char vol, char min, char max, String cname, String suffix) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(5);
  SendByte(cname.length() + suffix.length() + 6);
  SendByte(5);
  SendByte(vol);
  SendByte(min);
  SendByte(max);
  SendString(cname);
  SendString(suffix);
}

void DUILib::addSliderUInt16(byte channel, word vol, word min, word max, String cname, String suffix) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(5);
  SendByte(cname.length() + suffix.length() + 9);
  SendByte(6);
  SendWord(vol);
  SendWord(min);
  SendWord(max);
  SendString(cname);
  SendString(suffix);
}

void DUILib::addSliderInt16(byte channel, int vol, int min, int max, String cname, String suffix) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(5);
  SendByte(cname.length() + suffix.length() + 9);
  SendByte(7);
  SendInt(vol);
  SendInt(min);
  SendInt(max);
  SendString(cname);
  SendString(suffix);
}

void DUILib::addSliderInt32(byte channel, long vol, long min, long max, String cname, String suffix) {

  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(5);
  SendByte(cname.length() + suffix.length() + 15);
  SendByte(8);
  SendLong(vol);
  SendLong(min);
  SendLong(max);
  SendString(cname);
  SendString(suffix);
}
//=========================================
void DUILib::addCheckBox(byte channel, bool vol, String cname) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(6);
  SendByte(cname.length() + 2);
  SendByte(vol);
  SendString(cname);
}

void DUILib::addTextEdit(byte channel, String text, String cname) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(7);
  SendByte(text.length() + cname.length() + 2);
  SendString(text);
  SendString(cname);
}

void DUILib::addButton(byte channel, String cname) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(8);
  SendByte(cname.length() + 2);
  SendByte(1);
  SendString(cname);
}

void DUILib::addTwoButtons(byte channel, String b1name, String b2name) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(8);
  SendByte(b1name.length() + b2name.length() + 3);
  SendByte(2);
  SendString(b1name);
  SendString(b2name);
}

void DUILib::addThreeButtons(byte channel, String b1name, String b2name, String b3name) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(8);
  SendByte(b1name.length() + b2name.length() + b3name.length() + 4);
  SendByte(3);
  SendString(b1name);
  SendString(b2name);
  SendString(b3name);   
}

void DUILib::addEditTemperature(byte channel, byte H, byte L, String cname) {
  SendStart();
  SendByte(255);
  SendByte(channel);
  SendByte(9);
  SendByte(cname.length() + 3);
  SendByte(H);
  SendByte(L);
  SendString(cname);
}

//============PRIVATE==================================

unsigned int DUILib::make16(byte a, byte b)
{
  unsigned int ret = 0;
  ret = a;
  ret = (ret << 8);
  ret += b;
  return ret;
}

void DUILib::SendStart()
{
  Serial.write(253);
}

void DUILib::SendByte(byte data)
{
  if (data < 253) Serial.write(data);
  else {
    Serial.write(255);
    Serial.write(data - 128);
  }
}

void DUILib::SendWord(word data)
{
  SendByte(highByte(data));
  SendByte(lowByte(data));
}

void DUILib::SendInt(int data) {
  typedef volatile union {
    int i;
    byte w[2];
  } t_c;
  t_c c;
  c.i = data;
  SendByte(c.w[1]);
  SendByte(c.w[0]);
}

void DUILib::SendULong(unsigned long data) {
  typedef volatile union {
    unsigned long i;
    byte w[4];
  } t_c;
  t_c c;
  c.i = data;
  SendByte(c.w[3]);
  SendByte(c.w[2]);
  SendByte(c.w[1]);
  SendByte(c.w[0]);
}

void DUILib::SendLong(long data) {
    typedef volatile union {
    long i;
    byte w[4];
  } t_c;
  t_c c;
  c.i = data;
  SendByte(c.w[3]);
  SendByte(c.w[2]);
  SendByte(c.w[1]);
  SendByte(c.w[0]);
}

void DUILib::SendData(byte dsize, const byte *data)
{
  byte i = 0;
  SendByte(dsize);
  for (i = 0; i < dsize; i++) SendByte(data[i]);
}

void DUILib::SendString(String str) {
  int l = str.length();
  SendByte(l);
  for (int i = 0; i < l; i = i + 1) SendByte(str[i]);
}

void DUILib::SendPacket(byte channel, byte dsize, const byte *data)
{
  SendStart();
  SendByte(channel);
  SendData(dsize, data);
}

void DUILib::Analysis(byte data)
{
  if (data == 253)
  {
    rx_message.analysisState = 1;
    return ;
  }
  
  if (data == 255)
    rx_message.analysisState += 50;
  else if (rx_message.analysisState >= 50)
  {
    rx_message.analysisState -= 50;
    data += 128;
  };

  switch (rx_message.analysisState)
  {
    case 1:
        rx_message.channel = data;
        rx_message.analysisState = 2;
        if (rx_message.channel == 255) {
          rx_message.analysisState = 0;
          TransmitEnable = 1;
          (*OnFormInit)();
        }
        if (rx_message.channel == 254) {
          rx_message.analysisState = 0;
          TransmitEnable = 0;
        }
        break;

    // ===data ===
    case 2:         
        rx_message.dlc = data;
        rx_message.dps = 0;
        if (rx_message.dlc == 0) rx_message.analysisState = 4;
        else rx_message.analysisState = 3;
        break;
    case 3:
        rx_message.data[rx_message.dps] = data;
        rx_message.dps ++;
        if (rx_message.dps == rx_message.dlc) {
                  rx_message.analysisState = 0;
        (*OnReadData)(rx_message.channel, rx_message.data);
        }
        break;
  }
}



