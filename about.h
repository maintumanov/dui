#ifndef ABOUT_H
#define ABOUT_H

#include <QDialog>
#include <QDate>

namespace Ui {
class About;
}

class About : public QDialog
{
    Q_OBJECT

public:
    explicit About(QWidget *parent = Q_NULLPTR);
    ~About();

private:
    Ui::About *ui;
};

#endif // ABOUT_H
