#include "duiitemled.h"

duiItemLed::duiItemLed(quint8 channel, QByteArray *data, QWidget *parent) : duiItemAbstract(channel, parent)
{
    indicator = new QLedIndicator(this);
    indicator->setEnabled(false);
    initialization(data);
    setAutoFillBackground(true);
    QLabel *nameLabel = new QLabel(getName());
    QFont font = nameLabel->font();
    font.setPointSize(font.pointSize() + 3);
    nameLabel->setFont(font);

    QHBoxLayout *widgetLayout = new QHBoxLayout(this);
    widgetLayout->setMargin(0);
    widgetLayout->setSpacing(0);
    widgetLayout->addWidget(&frame);

    QHBoxLayout *valueLayout = new QHBoxLayout(&frame);
    QSpacerItem *spaceItem = new QSpacerItem(2,2, QSizePolicy::Expanding, QSizePolicy::Minimum);
    valueLayout->setMargin(2);
    valueLayout->setSpacing(2);
    valueLayout->addWidget(indicator);
    valueLayout->addWidget(nameLabel);

    valueLayout->addItem(spaceItem);
    //connect(this, SIGNAL(changeLabel(QString)), volueLabel, SLOT(setText(QString)));
}

void duiItemLed::reciveData(QByteArray Data)
{
    indicator->setChecked(QDUIRAWtoBool(&Data, 1));
}

void duiItemLed::initialization(QByteArray *data)
{
    QByteArray param = *data;
    setColorIndex(QDUIRAWtoByte(&param, 0, true));
    setName(QDUIRAWtoASCII(&param, 0, true));

}

void duiItemLed::setColorIndex(int indexColor)
{
    switch (indexColor) {
    case 0:
        indicator->setOnColor1(QColor(0,255,0));
        indicator->setOnColor2(QColor(0,192,0));
        indicator->setOffColor1(QColor(0,28,0));
        indicator->setOffColor2(QColor(0,128,0));
        break;
    case 1:
        indicator->setOnColor1(QColor(255,0,0));
        indicator->setOnColor2(QColor(192,0,0));
        indicator->setOffColor1(QColor(28,0,0));
        indicator->setOffColor2(QColor(128,0,0));
        break;
    case 2:
        indicator->setOnColor1(QColor(0,0,255));
        indicator->setOnColor2(QColor(0,0,220));
        indicator->setOffColor1(QColor(0,0,28));
        indicator->setOffColor2(QColor(0,0,128));
        break;
    case 3:
        indicator->setOnColor1(QColor(0,255,255));
        indicator->setOnColor2(QColor(0,192,192));
        indicator->setOffColor1(QColor(0,28,28));
        indicator->setOffColor2(QColor(0,128,128));
        break;
    case 4:
        indicator->setOnColor1(QColor(255,0,255));
        indicator->setOnColor2(QColor(192,0,192));
        indicator->setOffColor1(QColor(28,0,28));
        indicator->setOffColor2(QColor(128,0,128));
        break;
    case 5:
        indicator->setOnColor1(QColor(255,255,0));
        indicator->setOnColor2(QColor(192,192,0));
        indicator->setOffColor1(QColor(28,28,0));
        indicator->setOffColor2(QColor(128,128,0));
        break;
    case 6:
        indicator->setOnColor1(QColor(255,255,255));
        indicator->setOnColor2(QColor(192,192,192));
        indicator->setOffColor1(QColor(28,28,28));
        indicator->setOffColor2(QColor(128,128,128));
        break;
    }
}

