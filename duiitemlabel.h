#ifndef DUIITEMLABEL_H
#define DUIITEMLABEL_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QFrame>
#include <QDebug>
#include "duiitemabstract.h"
#include "qduishapes.h"

class duiItemLabel : public duiItemAbstract
{
    Q_OBJECT
public:
    explicit duiItemLabel(quint8 channel, QByteArray *data, QWidget *parent = 0);

signals:
    void changeLabel(QString text);

public slots:
    void reciveData(QByteArray Data);


private:
    QLabel *volueLabel;
    void initialization(QByteArray *data);
};

#endif // DUIITEMLABEL_H
