#include "duiitemchart.h"


duiItemChart::duiItemChart(quint8 channel, QByteArray *data, QWidget *parent) :
    duiItemAbstract(channel, parent)
{
    graph = new QSimpleTickerGraph();
    initialization(data);
    graph->setUnits(getSuffix());
    graph->setRange(0, 1);
    graph->setDataLinePen(QPen(QColor(128,64,64), 2));
  //  graph->setBackgroundBrush(QBrush(QColor(255, 255, 255)));
  //  graph->setBackgroundRole(QPalette::Window);
    graph->setGridPitch(50);
    graph->setGridPen(QPen(QColor(200, 200, 200), 1, Qt::DotLine));
    graph->setAxisColor(QColor(65,64,128));
    graph->setLabelColor(palette().text().color());
    graph->setPointWidth(2);
    graph->setReferencePoints(QList<double>() << 0);

    QLabel *nameLabel = new QLabel(getName());
    nameLabel->setAlignment(Qt::AlignHCenter);
    nameLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
    QFont font = nameLabel->font();
    font.setPointSize(font.pointSize() + 3);
    nameLabel->setFont(font);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);
    mainLayout->addWidget(&frame);
    setLayout(mainLayout);
    QVBoxLayout *valueLayout = new QVBoxLayout(&frame);
    valueLayout->setMargin(2);
    valueLayout->setSpacing(2);
    valueLayout->addWidget(nameLabel);
    valueLayout->addWidget(graph);
    graph->setRange(0, 100);
    dataType = 5;
    connect(graph, SIGNAL(mouseClick()), this, SLOT(click()));
    QPointF range = QDUIDataIndexRange(dataType);
    graph->setRange(range.x(), range.y());
}

duiItemChart::~duiItemChart()
{

}

void duiItemChart::reciveData(QByteArray Data)
{
    if (Data.count() < 2) return;
    qreal y = QDUIRAWtoReal(&Data);
    if (dataType != Data.at(0)) {
        dataType = Data.at(0);
        QPointF range = QDUIDataIndexRange(dataType);
        graph->setRange(range.x(), range.y());
    }
    graph->appendPoint(y);

    if (graph->range().second < y) graph->setRange(graph->range().first, y);
    if (graph->range().first > y) graph->setRange(y, graph->range().second);
}

void duiItemChart::click()
{
        QPointF range = QDUIDataIndexRange(dataType);
        graph->setRange(range.x(), range.y());
}

void duiItemChart::initialization(QByteArray *data)
{
    QByteArray param = *data;
    setName(QDUIRAWtoASCII(&param, 0, true));
    setSuffix(QDUIRAWtoASCII(&param, 0, true));
}

