#ifndef DUIITEMCHART_H
#define DUIITEMCHART_H

#include <QWidget>
#include <QFrame>
#include <QVBoxLayout>
#include <QLabel>
#include <QDebug>

#include "qsimpletickergraph.h"
#include "duiitemabstract.h"
#include "qduishapes.h"


class duiItemChart : public duiItemAbstract
{
    Q_OBJECT

public:
    explicit duiItemChart(quint8 channel, QByteArray *data, QWidget *parent = 0);
    ~duiItemChart();

public slots:
    void reciveData(QByteArray Data);
    void click();

private:
    QSimpleTickerGraph *graph;
    quint8 dataType;
    void initialization(QByteArray *data);
};

#endif // DUIITEMCHART_H
