#include "duiitemeditcheck.h"

duiItemEditCheck::duiItemEditCheck(quint8 channel, QByteArray *data, QWidget *parent) : duiItemAbstract(channel, parent)
{
    initialization(data);
    setAutoFillBackground(true);

    volueCheck = new QCheckBox(getName());
    volueCheck->setChecked(vol);
    volueCheck->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);


    QFont font = volueCheck->font();
    font.setPointSize(font.pointSize() + 3);
    volueCheck->setFont(font);

    QHBoxLayout *widgetLayout = new QHBoxLayout(this);
    widgetLayout->setMargin(0);
    widgetLayout->setSpacing(0);
    widgetLayout->addWidget(&frame);

    QHBoxLayout *valueLayout = new QHBoxLayout(&frame);
    valueLayout->setMargin(2);
    valueLayout->setSpacing(2);
    valueLayout->addWidget(volueCheck);
    connect(volueCheck, SIGNAL(toggled(bool)), this, SLOT(volueChanged(bool)));
    connect(this, SIGNAL(volEditChange(bool)), volueCheck, SLOT(setChecked(bool)));

}

void duiItemEditCheck::reciveData(QByteArray Data)
{
    volEditChange(QDUIRAWtoBool(&Data, 1, false));
}

void duiItemEditCheck::volueChanged(bool vol)
{
    QByteArray param;
    QDUIByteToRAW(&param, 0, 0);
    QDUIByteToRAW(&param, 1, vol);
    sendData(getChannel(), param);
}

void duiItemEditCheck::initialization(QByteArray *data)
{
    QByteArray param = *data;
    vol = QDUIRAWtoBool(&param, 0, true);
    setName(QDUIRAWtoASCII(&param, 0, true));
}

