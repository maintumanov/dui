#ifndef DUIITEMEDITSTRING_H
#define DUIITEMEDITSTRING_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QFrame>
#include <QDebug>
#include <QLineEdit>
#include "duiitemabstract.h"
#include "qduishapes.h"

class duiItemEditString : public duiItemAbstract
{
    Q_OBJECT
public:
    explicit duiItemEditString(quint8 channel, QByteArray *data, QWidget *parent = 0);

signals:
    void textChange(QString text);

public slots:
    void reciveData(QByteArray Data);

private slots:
    void volueChanged(QString text);

private:
    QLineEdit *textEdit;
    QString text;
   // quint8 maxLengthText;

    void initialization(QByteArray *data);
};

#endif // DUIITEMEDITSTRING_H
