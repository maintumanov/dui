#include "qduishapes.h"
#include <QDebug>

bool QDUICelsius = true;


QString QDUITypeRAWtoString(QByteArray *Data, QString suffix)
{
    if (Data->count() == 0) Data->append((char)0);

    switch (Data->at(0))
    {
    case 0:
        if (QDUIRAWtoBool(Data, 1)) return QObject::tr("TRUE");
        return QObject::tr("FALSE");
    case 1:
        if (QDUIRAWtoBool(Data, 1)) return QObject::tr("ON");
        return QObject::tr("OFF");
    case 2:
        if (QDUIRAWtoBool(Data, 1)) return QObject::tr("YES");
        return QObject::tr("NO");
    case 3:
        if (QDUIRAWtoBool(Data, 1)) return QObject::tr("OPEN");
        return QObject::tr("CLOSE");
    case 4:
        return QString("%1 %2").arg(QDUIRAWtoByte(Data, 1)).arg(suffix);
    case 5:
        return QString("%1 %2").arg(QDUIRAWtoInt8(Data, 1)).arg(suffix);
    case 6:
        return QString("%1 %2").arg(QDUIRAWtoUInt16(Data, 1)).arg(suffix);
    case 7:
        return QString("%1 %2").arg(QDUIRAWtoInt16(Data, 1)).arg(suffix);
    case 8:
        return QString("%1 %2").arg(QDUIRAWtoInt32(Data, 1)).arg(suffix);
    case 9:
        return QString("%1 %2").arg(QDUIRAWtoUInt32(Data, 1)).arg(suffix);
    case 10:
        return QString("%1 %2").arg(QDUIRAWtoTemperature(Data, 1), 0, 'f', 2).arg(" C");
    case 11:
        return QString("%1").arg(QDUIRAWtoDate(Data, 1).toString(QLatin1String("dd.MM.yyyy")));
    case 12:
        return QString("%1").arg(QDUIRAWtoTime(Data, 1).toString(QLatin1String("hh:mm:ss")));
    case 13:
        return QDUIRAWtoASCII(Data, 1, Data->count() - 1);
    case 14:
        return QString("%1").arg(QDUIRAWtoByte(Data, 1), 8, 2, QLatin1Char('0'));
    }
    return QString();
}

QPointF QDUIDataIndexRange(quint8 index)
{
    switch (index)
    {
    case 0: return QPointF(0, 1);
    case 1: return QPointF(0, 1);
    case 2: return QPointF(0, 1);
    case 3: return QPointF(0, 1);
    case 4: return QPointF(0, 255);
    case 5: return QPointF(-128, 128);
    case 6: return QPointF(0, 65535);
    case 7: return QPointF(-32768, 32768);
    case 8: return QPointF(-2147483648, 2147483648);
    case 9: return QPointF(0, 4294967296);
    case 10: return QPointF(-56, 127);
    case 11: return QPointF(0, 366);
    case 12: return QPointF(0, 86400);
    case 13: return QPointF(0, 0);
    case 14: return QPointF(0, 255);
    }
    return QPointF(-1, 1);
}


qint64 QDUIRAWtoInt64(QByteArray *data, bool cut)
{
    switch (data->at(0))
    {
    case 0: return QDUIRAWtoBool(data, 1, cut);
    case 1: return QDUIRAWtoBool(data, 1, cut);
    case 2: return QDUIRAWtoBool(data, 1, cut);
    case 3: return QDUIRAWtoBool(data, 1, cut);
    case 4: return QDUIRAWtoByte(data, 1, cut);
    case 5: return QDUIRAWtoInt8(data, 1, cut);
    case 6: return QDUIRAWtoUInt16(data, 1, cut);
    case 7: return QDUIRAWtoInt16(data, 1, cut);
    case 9: return QDUIRAWtoInt32(data, 1, cut);
    case 8: return QDUIRAWtoUInt32(data, 1, cut);
    case 10: return QDUIRAWtoTemperature(data, 1, cut);
    case 11: return QDUIRAWtoDate(data, 1, cut).dayOfYear();
    case 12: return QDUIRAWtoTime(data, 1, cut).msecsSinceStartOfDay() / 1000;
    case 13: return 0;
    case 14: return QDUIRAWtoByte(data, 1, cut);
    }
    if (cut) data->remove(0,1);
    return 0;
}

qreal QDUIRAWtoReal(QByteArray *data, bool cut)
{
    switch (data->at(0))
    {
    case 0: return QDUIRAWtoBool(data, 1, cut);
    case 1: return QDUIRAWtoBool(data, 1, cut);
    case 2: return QDUIRAWtoBool(data, 1, cut);
    case 3: return QDUIRAWtoBool(data, 1, cut);
    case 4: return QDUIRAWtoByte(data, 1, cut);
    case 5: return QDUIRAWtoInt8(data, 1, cut);
    case 6: return QDUIRAWtoUInt16(data, 1, cut);
    case 7: return QDUIRAWtoInt16(data, 1, cut);
    case 8: return QDUIRAWtoInt32(data, 1, cut);
    case 9: return QDUIRAWtoUInt32(data, 1, cut);
    case 10: return QDUIRAWtoTemperature(data, 1, cut);
    case 11: return QDUIRAWtoDate(data, 1, cut).dayOfYear();
    case 12: return QDUIRAWtoTime(data, 1, cut).msecsSinceStartOfDay() / 1000;
    case 13: return 0;
    case 14: return QDUIRAWtoByte(data, 1, cut);
    }
    if (cut) data->remove(0,1);
    return 0;
}

qreal QDUICelsiusToFahrenheit(qreal celsius)
{
    return 1.8 * celsius + 32;
}

qreal QDUIFahrenheitToCelsius(qreal fahrenheit)
{
    return (fahrenheit - 32) / 1.8;
}

//==============================


bool QDUIRAWtoBool(QByteArray *Data, int addr, bool cut)
{
    if (Data->count() <= addr) return false;
    bool ret = ((quint8)Data->at(addr) > 0);;
    if (cut) Data->remove(addr,1);
    return ret;
}

void QDUIBoolToRAW(QByteArray *Data, int addr, bool state)
{
    if (Data->count() <= addr) Data->resize(addr + 1);
    if (state) (*Data)[addr] = (quint8)1;
    else (*Data)[addr] = (quint8)0;
}

quint8 QDUIRAWtoByte(QByteArray *Data, int addr, bool cut)
{
    if (Data->count() <= addr) return 0;
    quint8 ret = (quint8)Data->at(addr);
    if (cut) Data->remove(addr,1);
    return ret;
}

void QDUIByteToRAW(QByteArray *Data, int addr, quint8 value)
{
    if (Data->count() <= addr) Data->resize(addr + 1);
    (*Data)[addr] = value;
}

qint8 QDUIRAWtoInt8(QByteArray *Data, int addr, bool cut)
{
    if (Data->count() <= addr) return 0;
    union t_u {qint8 vol; quint8 D;};
    t_u t;
    t.D = (quint8)Data->at(addr);
    if (cut) Data->remove(addr,1);
    return t.vol;
}

void QDUIInt8ToRAW(QByteArray *Data, int addr, qint8 value)
{
    if (Data->count() <= addr) Data->resize(addr + 1);
    union t_u {qint8 vol; quint8 D;};
    t_u t;
    t.vol = value;
    (*Data)[addr] = t.D;
}

quint16 QDUIRAWtoUInt16(QByteArray *Data, int addr, bool cut)
{
    if (Data->count() <= addr + 1) return 0;
    union t_u {quint16 vol; quint8 D[2];};
    t_u t;
    t.D[0] = (quint8)Data->at(addr + 1);
    t.D[1] = (quint8)Data->at(addr);
    if (cut) Data->remove(addr,2);
    return t.vol;
}

void QDUIUInt16ToRAW(QByteArray *Data, int addr, quint16 value)
{
    if (Data->count() <= addr + 1) Data->resize(addr + 2);
    union t_u {quint16 vol; quint8 D[2];};
    t_u t;
    t.vol = value;
    (*Data)[addr + 1] = t.D[0];
    (*Data)[addr] = t.D[1];
}

qint16 QDUIRAWtoInt16(QByteArray *Data, int addr, bool cut)
{
    if (Data->count() <= addr + 1) return 0;
    union t_u {qint16 vol; quint8 D[2];};
    t_u t;
    t.D[0] = (quint8)Data->at(addr + 1);
    t.D[1] = (quint8)Data->at(addr);
    if (cut) Data->remove(addr,2);
    return t.vol;
}

void QDUIInt16ToRAW(QByteArray *Data, int addr, qint16 value)
{
    if (Data->count() <= addr + 1) Data->resize(addr + 2);
    union t_u {qint16 vol; quint8 D[2];};
    t_u t;
    t.vol = value;
    (*Data)[addr + 1] = t.D[0];
    (*Data)[addr] = t.D[1];
}

qint32 QDUIRAWtoInt32(QByteArray *Data, int addr, bool cut)
{
    if (Data->count() <= addr + 3) return 0;
    union t_u {qint32 vol; quint8 D[4];};
    t_u t;
    for (int i = 0; i < 4; i ++)
        t.D[i] = (quint8)Data->at(addr + 3 - i);
    if (cut) Data->remove(addr,3);
    return t.vol;
}

void QDUIInt32ToRAW(QByteArray *Data, int addr, qint32 value)
{
    if (Data->count() <= addr + 3) Data->resize(addr + 4);
    union t_u {qint32 vol; quint8 D[4];};
    t_u t;
    t.vol = value;
    for (int i = 0; i < 4; i ++)
        (*Data)[addr + 3 - i] = t.D[i];
}

quint32 QDUIRAWtoUInt32(QByteArray *Data, int addr, bool cut)
{
    if (Data->count() <= addr + 3) return 0;
    union t_u {quint32 vol; quint8 D[4];};
    t_u t;
    for (int i = 0; i < 4; i ++)
        t.D[i] = (quint8)Data->at(addr + 3 - i);
    if (cut) Data->remove(addr,3);
    return t.vol;
}

void QDUIUInt32ToRAW(QByteArray *Data, int addr, quint32 value)
{
    if (Data->count() <= addr + 3) Data->resize(addr + 4);
    union t_u {quint32 vol; quint8 D[4];};
    t_u t;
    t.vol = value;
    for (int i = 0; i < 4; i ++)
        (*Data)[addr + 3 - i] = t.D[i];
}

qreal QDUIRAWtoTemperature(QByteArray *Data, int addr, bool cut)
{
    if (Data->count() <= addr + 1) return 0;
    quint16 vl = (quint8)Data->at(addr);
    qreal temp;
    vl = (vl << 8);
    vl += (quint8)Data->at(addr + 1);
    if ((vl >> 15) == 0) {
        temp = (vl * 0.00390625);
    } else {
        vl = 0xFFFF^vl;
        vl ++;
        temp = (vl * -0.00390625);
    }
    if (cut) Data->remove(addr,2);
    if (QDUICelsius) return temp;
    else return QDUICelsiusToFahrenheit(temp);
}

void QDUITemperatureToRAW(QByteArray *Data, int addr, qreal value)
{
    if (Data->count() <= addr + 1) Data->resize(addr + 2);
    quint16 cv;
    qreal temp;
    if (QDUICelsius) temp = value;
    else temp = QDUIFahrenheitToCelsius(value);
    if (value < 0) {
        cv = temp / -0.00390625;
        cv --;
        cv = 0xFFFF ^ cv;
        (*Data)[addr] = (cv >> 8);
        (*Data)[addr + 1] = (cv & 255);

    } else {
        cv = temp / 0.00390625;
        (*Data)[addr] = (cv >> 8);
        (*Data)[addr + 1] = (cv & 255);
    }
}

QString QDUIRAWtoASCII(QByteArray *Data, int addr, bool cut)
{
    QByteArray ascii;
    int textLength = Data->at(addr);
    if (Data->count() < addr + textLength + 1) return ascii;
    for (int i = 0; i < textLength; i ++){
        if ((quint8)Data->at(addr + i + 1) == 0) return ascii;
        ascii.append((quint8)Data->at(addr + i + 1));
    }
    if (cut) Data->remove(addr,textLength + 1);
    return QString::fromUtf8(ascii.constData(), ascii.size());
}

void QDUIASCIIToRAW(QByteArray *Data, int addr, QString text)
{
    QByteArray ascii = text.toUtf8();
    (*Data)[addr] = ascii.count();
    if (Data->count() <= addr + text.count() - 1) Data->resize(addr + text.count());
    for (int i = 0; i < ascii.count(); i ++)
        (*Data)[addr + i + 1] = ascii[i];
}

QDate QDUIRAWtoDate(QByteArray *Data, int addr, bool cut)
{
    if (Data->count() <= addr + 3) return QDate();
    QDate date;
    int year;
    year = ((quint8)Data->at(addr + 2) << 8) + (quint8)Data->at(addr + 3);
    date.setDate(year, (quint8)Data->at(addr + 1), (quint8)Data->at(addr));
    if (cut) Data->remove(addr, 4);
    return date;
}

void QDUIDateToRAW(QByteArray *Data, int addr, QDate date)
{
    if (Data->count() <= addr + 3) Data->resize(addr + 4);
    quint16 year = date.year();
    (*Data)[addr] = (date.day());
    (*Data)[addr + 1] = (date.month());
    QDUIUInt16ToRAW(Data, addr + 2, year);
}

QTime QDUIRAWtoTime(QByteArray *Data, int addr, bool cut)
{
    if (Data->count() <= addr + 2) return QTime();
    QTime time;
    time.setHMS((quint8)Data->at(addr), (quint8)Data->at(addr + 1), (quint8)Data->at(addr + 2));
    if (cut) Data->remove(addr, 3);
    return time;
}

void QDUITimeToRAW(QByteArray *Data, int addr, QTime time)
{
    if (Data->count() <= addr + 2) Data->resize(addr + 3);
    (*Data)[addr] = (time.hour());
    (*Data)[addr + 1] = (time.minute());
    (*Data)[addr + 2] = (time.second());
}

QDir QDUIHomeDir()
{
    QDir HomeDir = QDir::home();
    const QString Folder = QLatin1String("signalnet");
    if (!HomeDir.exists(Folder)) HomeDir.mkpath(Folder);
    HomeDir.cd(Folder);
    return HomeDir;
}

QString QDUINormalizationFileName(QString name)
{
    QString ret = name.toLower();
    for (int i = 0; i < ret.count(); i ++)
    {
        if (ret[i] == ' ') ret[i] = '_';
        if (ret[i] == ',') ret[i] = '_';
        if (ret[i] == '.') ret[i] = '_';
        if (ret[i] == ';') ret[i] = '_';
        if (ret[i] == ':') ret[i] = '_';
    }
    return ret;
}

