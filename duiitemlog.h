#ifndef DUIITEMLOG_H
#define DUIITEMLOG_H

#include <QWidget>
#include <QFrame>
#include <QVBoxLayout>
#include <QLabel>
#include <QDebug>
#include <QListWidget>
#include <QPushButton>

#include "duiitemabstract.h"
#include "qduishapes.h"


class duiItemLog : public duiItemAbstract
{
    Q_OBJECT

public:
    explicit duiItemLog(quint8 channel, QByteArray *data, QWidget *parent = Q_NULLPTR);
    ~duiItemLog();

public slots:
    void reciveData(QByteArray Data);
    void clear();
    void plus();
    void minus();

private:
    QListWidget *log;
    QPushButton *pbutton;
    QPushButton *pbuttonp;
    QPushButton *pbuttonm;
    int lines;
    quint8 dataType;
    void initialization(QByteArray *data);
    void setLines(int l);
    void addLog(QString text);

};

#endif // DUIITEMLOG_H
