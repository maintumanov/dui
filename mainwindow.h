#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFrame>
#include <QCheckBox>
#include <QtSerialPort/QSerialPortInfo>
#include "qdui.h"
#include "duiitemabstract.h"
#include "duiitemlabel.h"
#include "duiitemled.h"
#include "duiitemchart.h"
#include "duiitemeditint.h"
#include "duiitemeditslider.h"
#include "duiitemeditcheck.h"
#include "duiitemeditstring.h"
#include "duiitemeditbutton.h"
#include "duiitemedittemperature.h"
#include "duiitemlog.h"
#include "about.h"
#include "qduishapes.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT


public:
    explicit MainWindow(QWidget *parent = Q_NULLPTR);
    ~MainWindow();

    QDUI *DUI;
    void closeEvent(QCloseEvent *event);
    
private slots:
    void on_button_close_clicked();
    void stateConnectChange(bool state);
    void set_mode_gui(int mode);

    void error_connect();
    void showMessage(int importance, QString msg);
    void showMessageTimeOut();
    void setFrameColor(int importance, QFrame *frame);
    void loadSettings();
    void saveSettings();
    void fillPortsInfo();
    void changeSelectedPort(int index);
    void on_button_info_clicked();

    void reciveData(quint8 Channel, QByteArray Data);
    void reciveInit(quint8 channel, quint8 type, QByteArray Data);

    void clearItems();

private:
    double vConst;
    Ui::MainWindow *ui;
    quint8 programState;
    QTimer *msgTimer;
    QList<duiItemAbstract*> items;



};

#endif // MAINWINDOW_H
