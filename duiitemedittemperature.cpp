#include "duiitemedittemperature.h"

duiItemEditTemperature::duiItemEditTemperature(quint8 channel, QByteArray *data, QWidget *parent) : duiItemAbstract(channel, parent)
{
    initialization(data);
    setAutoFillBackground(true);
    QLabel *nameLabel = new QLabel(getName() + ":");
    QFont font = nameLabel->font();
    font.setPointSize(font.pointSize() + 3);
    nameLabel->setFont(font);

    QHBoxLayout *widgetLayout = new QHBoxLayout(this);
    widgetLayout->setMargin(0);
    widgetLayout->setSpacing(0);
    widgetLayout->addWidget(&frame);

    volueEdit = new QDoubleSpinBox();
    volueEdit->setMaximum(127);
    volueEdit->setMinimum(-127);
    volueEdit->setValue(vol);
    volueEdit->setSuffix(" C");
    volueEdit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    QHBoxLayout *valueLayout = new QHBoxLayout(&frame);
    valueLayout->setMargin(2);
    valueLayout->setSpacing(2);
    valueLayout->addWidget(nameLabel);
    valueLayout->addWidget(volueEdit);
    connect(volueEdit, SIGNAL(valueChanged(double)), this, SLOT(volueChanged(double)));
    connect(this, SIGNAL(volEditChange(double)), volueEdit, SLOT(setValue(double)));
}

void duiItemEditTemperature::reciveData(QByteArray Data)
{
    volEditChange(QDUIRAWtoTemperature(&Data, 1, false));
}

void duiItemEditTemperature::volueChanged(double vol)
{
    QByteArray param;
    QDUIByteToRAW(&param, 0, 10);
    QDUITemperatureToRAW(&param, 1, vol);
    sendData(getChannel(), param);
}

void duiItemEditTemperature::initialization(QByteArray *data)
{
    QByteArray param = *data;
    vol = QDUIRAWtoTemperature(&param, 0, true);
    setName(QDUIRAWtoASCII(&param, 0, true));
}

