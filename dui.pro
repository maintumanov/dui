#-------------------------------------------------
#
# Project created by QtCreator 2016-07-25T11:28:01
#
#-------------------------------------------------

QT       += core serialport widgets

TARGET = DUI
TEMPLATE = app

SOURCES += main.cpp\
    mainwindow.cpp \
    qdui.cpp \
    about.cpp \
    duiitemabstract.cpp \
    duiitemlabel.cpp \
    qduishapes.cpp \
    duiitemchart.cpp \
    duiitemled.cpp \
    qledindicator.cpp \
    duiitemeditint.cpp \
    duiitemeditslider.cpp \
    duiitemeditcheck.cpp \
    duiitemeditstring.cpp \
    duiitemeditbutton.cpp \
    duiitemedittemperature.cpp \
    qsimpletickergraph.cpp \
    duiitemlog.cpp

HEADERS  += mainwindow.h \
    qdui.h \
    about.h \
    duiitemlabel.h \
    qduishapes.h \
    duiitemled.h \
    qledindicator.h \
    duiitemeditint.h \
    duiitemeditslider.h \
    duiitemeditstring.h \
    duiitemedittemperature.h \
    duiitemabstract.h \
    duiitemchart.h \
    duiitemeditbutton.h \
    duiitemeditcheck.h \
    qsimpletickergraph.h \
    duiitemlog.h

FORMS    += mainwindow.ui \
    about.ui

TRANSLATIONS = dui_ru_RU.ts

win32 {
        RC_FILE += applico.rc
        OTHER_FILES += applico.rc
}

RESOURCES += \
    resource.qrc
