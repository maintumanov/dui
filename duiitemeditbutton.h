#ifndef DUIITEMEDITBUTTON_H
#define DUIITEMEDITBUTTON_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QFrame>
#include <QDebug>
#include <QPushButton>
#include "duiitemabstract.h"
#include "qduishapes.h"

class duiItemEditButton : public duiItemAbstract
{
    Q_OBJECT
public:
    explicit duiItemEditButton(quint8 channel, QByteArray *data, QWidget *parent = 0);

signals:
    void button1Enable(bool enabled);
    void button2Enable(bool enabled);
    void button3Enable(bool enabled);

public slots:
    void reciveData(QByteArray Data);

private slots:
    void button1Click();
    void button2Click();
    void button3Click();

private:
    QPushButton *button1;
    QPushButton *button2;
    QPushButton *button3;
    int buttonsCount;
    QString b1Name;
    QString b2Name;
    QString b3Name;

    void initialization(QByteArray *data);
};

#endif // DUIITEMEDITBUTTON_H
