#include "duiitemlabel.h"

duiItemLabel::duiItemLabel(quint8 channel, QByteArray *data, QWidget *parent) : duiItemAbstract(channel, parent)
{
    initialization(data);
    setAutoFillBackground(true);
    QLabel *nameLabel = new QLabel(getName() + ":");
    QFont font = nameLabel->font();
    font.setPointSize(font.pointSize() + 3);
    nameLabel->setFont(font);

    QHBoxLayout *widgetLayout = new QHBoxLayout(this);
    widgetLayout->setMargin(0);
    widgetLayout->setSpacing(0);
    widgetLayout->addWidget(&frame);

    volueLabel = new QLabel(QString(tr("%1 %2")).arg(0).arg(getSuffix()));
    font.setPointSize(font.pointSize() + 1);
    volueLabel->setFont(font);
    QHBoxLayout *valueLayout = new QHBoxLayout(&frame);
    QSpacerItem *spaceItem = new QSpacerItem(2,2, QSizePolicy::Expanding, QSizePolicy::Minimum);
    valueLayout->setMargin(2);
    valueLayout->setSpacing(2);
    valueLayout->addWidget(nameLabel);
    valueLayout->addWidget(volueLabel);
    valueLayout->addItem(spaceItem);
    connect(this, SIGNAL(changeLabel(QString)), volueLabel, SLOT(setText(QString)));
}

void duiItemLabel::reciveData(QByteArray Data)
{
    changeLabel(QDUITypeRAWtoString(&Data, getSuffix()));
}

void duiItemLabel::initialization(QByteArray *data)
{
    QByteArray param = *data;
    QString str = QDUIRAWtoASCII(&param, 0, true);
    setName(str);
    str = QDUIRAWtoASCII(&param, 0, true);
    setSuffix(str);
}

