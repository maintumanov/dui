#include "duiitemeditstring.h"

duiItemEditString::duiItemEditString(quint8 channel, QByteArray *data, QWidget *parent) : duiItemAbstract(channel, parent)
{
    initialization(data);
    setAutoFillBackground(true);
    QLabel *nameLabel = new QLabel(getName() + ":");
    QFont font = nameLabel->font();
    font.setPointSize(font.pointSize() + 3);
    nameLabel->setFont(font);

    QHBoxLayout *widgetLayout = new QHBoxLayout(this);
    widgetLayout->setMargin(0);
    widgetLayout->setSpacing(0);
    widgetLayout->addWidget(&frame);

    textEdit = new QLineEdit(text);
    textEdit->setMaxLength(80);
    textEdit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    QHBoxLayout *valueLayout = new QHBoxLayout(&frame);
    valueLayout->setMargin(2);
    valueLayout->setSpacing(2);
    valueLayout->addWidget(nameLabel);
    valueLayout->addWidget(textEdit);
    connect(textEdit, SIGNAL(textChanged(QString)), this, SLOT(volueChanged(QString)));
    connect(this, SIGNAL(textChange(QString)), textEdit, SLOT(setText(QString)));

}

void duiItemEditString::reciveData(QByteArray Data)
{
    textChange(QDUIRAWtoASCII(&Data, 1, false));
}

void duiItemEditString::volueChanged(QString text)
{
    QByteArray param;
    QDUIByteToRAW(&param, 0, 13);
    QDUIASCIIToRAW(&param, 1, text);
    sendData(getChannel(), param);
}

void duiItemEditString::initialization(QByteArray *data)
{
    QByteArray param = *data;
    text = QDUIRAWtoASCII(&param, 0, true);
    setName(QDUIRAWtoASCII(&param, 0, true));
}

