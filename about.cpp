#include "about.h"
#include "ui_about.h"

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
    ui->label->setText(QString("<html><head/><body><p>"
                               "<span style=\" font-size:12pt; font-weight:600;\">%1 %2</span></p>"
                               "<p>%3</p>"
                               "<p>email: <a href=\"mailto:maintumanov@mail.ru\">maintumanov@mail.ru</a></p>"
                               "<p>2014-%4 The SignalNet company. <br/>%5</p></body></html>")

                       .arg(QCoreApplication::applicationName())
                       .arg(QCoreApplication::applicationVersion())
                       .arg(tr("Author: Tumanov Stanislav Aleksandrovich"))
                       .arg(2019)
                       .arg("The program is provided AS IS with NO WARRANTY OF ANY KIND, "
                            "INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS "
                            "FOR A PARTICULAR PURPOSE."));


    // ui->labelDate->setText(QString("Build %1 in %2").arg(__DATE__).arg(__TIME__));
    //ui->labelDate->setText(QString("%1").arg(__COUNTER__));
}

About::~About()
{
    delete ui;
}
