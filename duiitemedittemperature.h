#ifndef DUIITEMEDITTEMPERATURE_H
#define DUIITEMEDITTEMPERATURE_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QFrame>
#include <QDebug>
#include <QDoubleSpinBox>
#include "duiitemabstract.h"
#include "qduishapes.h"

class duiItemEditTemperature : public duiItemAbstract
{
    Q_OBJECT
public:
    explicit duiItemEditTemperature(quint8 channel, QByteArray *data, QWidget *parent = 0);

signals:
    void volEditChange(double vol);

public slots:
    void reciveData(QByteArray Data);

private slots:
    void volueChanged(double vol);

private:
    QDoubleSpinBox *volueEdit;
    double vol;

    void initialization(QByteArray *data);
};

#endif // DUIITEMEDITTEMPERATURE_H
