#ifndef DUIITEMEDITINT_H
#define DUIITEMEDITINT_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QFrame>
#include <QDebug>
#include <QSpinBox>
#include "duiitemabstract.h"
#include "qduishapes.h"

class duiItemEditInt : public duiItemAbstract
{
    Q_OBJECT
public:
    explicit duiItemEditInt(quint8 channel, QByteArray *data, QWidget *parent = 0);

signals:
    void volEditChange(int vol);

public slots:
    void reciveData(QByteArray Data);

private slots:
    void volueChanged(int vol);

private:
    QSpinBox *volueEdit;
    int vol;
    int volMax;
    int volMin;
    int volType;

    void initialization(QByteArray *data);
};

#endif // DUIITEMEDITINT_H
