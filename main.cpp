#include <QtWidgets/QApplication>
#include <QTranslator>
#include <QStyleFactory>
#include <QPalette>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator appTranslator;
    QCoreApplication::setOrganizationName(QLatin1String("SignalNet"));
    QCoreApplication::setApplicationName(QLatin1String("DUI"));
    QCoreApplication::setApplicationVersion("0.1.1.0");
    qApp->setStyle(QStyleFactory::create("Fusion"));

    QPalette dark_palette = QPalette();
    dark_palette.setColor(QPalette::Window, QColor(53, 53, 53));
    dark_palette.setColor(QPalette::WindowText, Qt::white);
    dark_palette.setColor(QPalette::Base, QColor(25, 25, 25));
    dark_palette.setColor(QPalette::AlternateBase, QColor(37, 25, 49));
    dark_palette.setColor(QPalette::ToolTipBase, Qt::white);
    dark_palette.setColor(QPalette::ToolTipText, Qt::white);
    dark_palette.setColor(QPalette::Text, Qt::white);
    dark_palette.setColor(QPalette::Button, QColor(53, 53, 53));
    dark_palette.setColor(QPalette::ButtonText, Qt::white);
    dark_palette.setColor(QPalette::BrightText, Qt::red);
    dark_palette.setColor(QPalette::Link, QColor(42, 130, 218));
    dark_palette.setColor(QPalette::Highlight, QColor(42, 130, 218));
    dark_palette.setColor(QPalette::HighlightedText, Qt::black);
    qApp->setPalette(dark_palette);
    qApp->setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }");

    //appTranslator.load(QLatin1String("dui_")+QLocale::system().name(), a.applicationDirPath());
    appTranslator.load(QLatin1String("dui_")+QLocale::system().name(),"://");

    a.installTranslator(&appTranslator);
    MainWindow w;
    w.show();
    
    return a.exec();
}
