#ifndef DUIITEMEDITSLIDER_H
#define DUIITEMEDITSLIDER_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QFrame>
#include <QDebug>
#include <QSlider>
#include "duiitemabstract.h"
#include "qduishapes.h"

class duiItemEditSlider : public duiItemAbstract
{
    Q_OBJECT
public:
    explicit duiItemEditSlider(quint8 channel, QByteArray *data, QWidget *parent = 0);

signals:
    void volEditChange(int vol);
    void volLabelChange(QString vol);

public slots:
    void reciveData(QByteArray Data);

private slots:
    void volueChanged(int vol);

private:
    QSlider *volueEdit;
    QLabel *vollabel;
    int vol;
    int volMax;
    int volMin;
    int volType;

    void initialization(QByteArray *data);
};

#endif // DUIITEMEDITSLIDER_H
