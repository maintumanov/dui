#ifndef DUIITEMLED_H
#define DUIITEMLED_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QFrame>
#include <QDebug>
#include <QPainter>
#include <QColor>
#include "duiitemabstract.h"
#include "qduishapes.h"
#include "qledindicator.h"

class duiItemLed : public duiItemAbstract
{
    Q_OBJECT
public:
    explicit duiItemLed(quint8 channel, QByteArray *data, QWidget *parent = 0);

public slots:
    void reciveData(QByteArray Data);

private:
    QLedIndicator *indicator;
    void initialization(QByteArray *data);
    void setColorIndex(int indexColor);


};

#endif // DUIITEMLED_H
