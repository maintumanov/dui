#include "duiitemeditslider.h"

duiItemEditSlider::duiItemEditSlider(quint8 channel, QByteArray *data, QWidget *parent) : duiItemAbstract(channel, parent)
{
    initialization(data);
    setAutoFillBackground(true);
    QLabel *nameLabel = new QLabel(getName() + ":");
    QFont font = nameLabel->font();
    font.setPointSize(font.pointSize() + 3);
    nameLabel->setFont(font);

    QHBoxLayout *widgetLayout = new QHBoxLayout(this);
    widgetLayout->setMargin(0);
    widgetLayout->setSpacing(0);
    widgetLayout->addWidget(&frame);

    volueEdit = new QSlider(Qt::Horizontal);
    volueEdit->setMaximum(volMax);
    volueEdit->setMinimum(volMin);
    volueEdit->setValue(vol);
    volueEdit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

    vollabel = new QLabel();
    vollabel->setText(QString("%1 %2").arg(vol).arg(getSuffix()));
    vollabel->setFont(font);
    vollabel->setAlignment(Qt::AlignRight);
    vollabel->setFixedWidth(vollabel->fontMetrics().width(QString("%1 %2").arg(volMax).arg(getSuffix())));
    QHBoxLayout *valueLayout = new QHBoxLayout(&frame);
    valueLayout->setMargin(2);
    valueLayout->setSpacing(2);
    valueLayout->addWidget(nameLabel);
    valueLayout->addWidget(volueEdit);
    valueLayout->addWidget(vollabel);
    connect(volueEdit, SIGNAL(valueChanged(int)), this, SLOT(volueChanged(int)));
    connect(this, SIGNAL(volEditChange(int)), volueEdit, SLOT(setValue(int)));
    connect(this, SIGNAL(volLabelChange(QString)), vollabel, SLOT(setText(QString)));
}

void duiItemEditSlider::reciveData(QByteArray Data)
{
    volEditChange(QDUIRAWtoInt64(&Data, false));
}

void duiItemEditSlider::volueChanged(int vol)
{
    QByteArray param;
    QDUIByteToRAW(&param, 0, volType);
    switch (volType) {
    case 4: QDUIByteToRAW(&param, 1, vol); break;
    case 5: QDUIInt8ToRAW(&param, 1, vol); break;
    case 6: QDUIUInt16ToRAW(&param, 1, vol); break;
    case 7: QDUIInt16ToRAW(&param, 1, vol); break;
    case 8: QDUIInt32ToRAW(&param, 1, vol); break;
    }
    sendData(getChannel(), param);
    volLabelChange(QString("%1 %2").arg(vol).arg(getSuffix()));
}

void duiItemEditSlider::initialization(QByteArray *data)
{
    QByteArray param = *data;
    volType = QDUIRAWtoByte(&param, 0, true);
    switch (volType) {
    case 4:
        vol = QDUIRAWtoByte(&param, 0, true);
        volMin = QDUIRAWtoByte(&param, 0, true);
        volMax = QDUIRAWtoByte(&param, 0, true);
        break;
    case 5:
        vol = QDUIRAWtoInt8(&param, 0, true);
        volMin = QDUIRAWtoInt8(&param, 0, true);
        volMax = QDUIRAWtoInt8(&param, 0, true);
        break;
    case 6:
        vol = QDUIRAWtoUInt16(&param, 0, true);
        volMin = QDUIRAWtoUInt16(&param, 0, true);
        volMax = QDUIRAWtoUInt16(&param, 0, true);
        break;
    case 7:
        vol = QDUIRAWtoInt16(&param, 0, true);
        volMin = QDUIRAWtoInt16(&param, 0, true);
        volMax = QDUIRAWtoInt16(&param, 0, true);
        break;
    case 8:
        vol = QDUIRAWtoInt32(&param, 0, true);
        volMin = QDUIRAWtoInt32(&param, 0, true);
        volMax = QDUIRAWtoInt32(&param, 0, true);
        break;
    }
    QString str = QDUIRAWtoASCII(&param, 0, true);
    setName(str);
    str = QDUIRAWtoASCII(&param, 0, true);
    setSuffix(str);
}

