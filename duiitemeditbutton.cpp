#include "duiitemeditbutton.h"

duiItemEditButton::duiItemEditButton(quint8 channel, QByteArray *data, QWidget *parent) : duiItemAbstract(channel, parent)
{
    initialization(data);
    setAutoFillBackground(true);

    QHBoxLayout *widgetLayout = new QHBoxLayout(this);
    widgetLayout->setMargin(0);
    widgetLayout->setSpacing(0);
    widgetLayout->addWidget(&frame);

    QHBoxLayout *valueLayout = new QHBoxLayout(&frame);
    valueLayout->setMargin(2);
    valueLayout->setSpacing(2);

    button1 = new QPushButton(b1Name);
    button1->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    valueLayout->addWidget(button1);
    connect(button1, SIGNAL(clicked(bool)), this, SLOT(button1Click()));
    connect(this, SIGNAL(button1Enable(bool)), button1, SLOT(setEnabled(bool)));

    if (buttonsCount > 1) {
        button2 = new QPushButton(b2Name);
        button2->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
        valueLayout->addWidget(button2);
        connect(button2, SIGNAL(clicked(bool)), this, SLOT(button2Click()));
        connect(this, SIGNAL(button1Enable(bool)), button1, SLOT(setEnabled(bool)));
    }

    if (buttonsCount > 2) {
        button3 = new QPushButton(b3Name);
        button3->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
        valueLayout->addWidget(button3);
        connect(button3, SIGNAL(clicked(bool)), this, SLOT(button3Click()));
        connect(this, SIGNAL(button1Enable(bool)), button1, SLOT(setEnabled(bool)));
    }

    QSpacerItem *spaceItem = new QSpacerItem(2,2, QSizePolicy::Expanding, QSizePolicy::Minimum);
    valueLayout->addItem(spaceItem);

}

void duiItemEditButton::reciveData(QByteArray Data)
{
    int button = QDUIRAWtoBool(&Data, 1, false);
    bool senable = false;
    if (button > 3) {
        senable = true;
        button = - 3;
    }
    switch (button) {
    case 1: button1Enable(senable); break;
    case 2: button2Enable(senable); break;
    case 3: button3Enable(senable); break;
    }
}

void duiItemEditButton::button1Click()
{
    QByteArray param;
    QDUIByteToRAW(&param, 0, 4);
    QDUIByteToRAW(&param, 1, 1);
    sendData(getChannel(), param);
}

void duiItemEditButton::button2Click()
{
    QByteArray param;
    QDUIByteToRAW(&param, 0, 4);
    QDUIByteToRAW(&param, 1, 2);
    sendData(getChannel(), param);
}

void duiItemEditButton::button3Click()
{
    QByteArray param;
    QDUIByteToRAW(&param, 0, 4);
    QDUIByteToRAW(&param, 1, 3);
    sendData(getChannel(), param);
}

void duiItemEditButton::initialization(QByteArray *data)
{
    QByteArray param = *data;
    buttonsCount = QDUIRAWtoByte(&param, 0, true);
    b1Name = QDUIRAWtoASCII(&param, 0, true);
    if (buttonsCount > 1) b2Name = QDUIRAWtoASCII(&param, 0, true);
    if (buttonsCount > 2) b3Name = QDUIRAWtoASCII(&param, 0, true);
}

