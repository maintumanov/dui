#ifndef DUIITEMABSTRACT_H
#define DUIITEMABSTRACT_H

#include <QWidget>
#include <QFrame>

class duiItemAbstract : public QWidget
{
    Q_OBJECT
public:
    explicit duiItemAbstract(quint8 channel, QWidget *parent = Q_NULLPTR);
    virtual QWidget *getWidget();
    void setName(QString name);
    void setSuffix(QString suffix);
    quint8 getChannel();
    virtual quint8 getType();
    QString getName();
    QString getSuffix();


signals:
    void sendData(quint8 Channel, QByteArray Data);

public slots:
    virtual void reciveData(QByteArray Data);

protected:
    QFrame frame;

private:
    quint8 ichannel;
    QString isuffix;
    QString iname;

};

#endif // DUIITEMABSTRACT_H
