#ifndef QDUI_H
#define QDUI_H

#include <QObject>
#include <QtCore>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QDebug>


struct QSNmessage
{
    quint16 type;
    quint8 channel;
    bool inititem;
    QString name;
    QString suffix;
    quint8 dataSize;
    QByteArray data;
};

struct QRMsg
{
    quint16 AnalysisState;
    QSNmessage Msg;
};

class QDUI : public QObject
{
    Q_OBJECT

private:
    QSerialPort *serial;
    QRMsg NewMsg;
    bool formRecived;
    void Analysis(quint8 data);
    void addStart(QByteArray *sendbuffer);
    void addByte(quint8 data, QByteArray *sendbuffer);
    void addWord(quint16 data, QByteArray *sendbuffer);
    void addData(QByteArray *data, QByteArray *sendbuffer);


public:
    explicit QDUI(QObject *parent = Q_NULLPTR);
    void SaveSettings(QSettings *settings);
    void LoadSettings(QSettings *settings);

signals:
    void reciveData(quint8 Channel, QByteArray Data);
    void reciveInit(quint8 channel, quint8 type, QByteArray Data);
    void connectStateChange(bool state);
    void reciveReinit();

private slots:
    void AnalisisCicle();
    void SendFormInit();
    void SendDisconnect();

public slots:
    void openPort();
    void closePort();
    void setPortName(QString name);
    void SendData(quint8 Channel, QByteArray Data);
//    void SendInt8(quint8 Channel, quint8 type, quint8 vol);
//    void SendInt16(quint8 Channel, quint8 type, quint16 vol);

};

#endif // QSNUART_H
