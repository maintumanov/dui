//edit 16.08.2016
#ifndef QDUISHAPES_H
#define QDUISHAPES_H

#include <QObject>
#include <QVector>
#include <QStringList>
#include <QDataStream>
#include <QDateTime>
#include <QDir>
#include <QPointF>
//#include <qdebug.h>

extern bool QDUICelsius;

struct QSNPower
{
    qint32 power;
    quint16 seconds;
};

QString QDUITypeRAWtoString(QByteArray *Data, QString suffix = QString());
QPointF QDUIDataIndexRange(quint8 index);
qint64 QDUIRAWtoInt64(QByteArray *data, bool cut = false);
qreal QDUIRAWtoReal(QByteArray *data, bool cut = false);

// conversion types
qreal QDUICelsiusToFahrenheit(qreal celsius);
qreal QDUIFahrenheitToCelsius(qreal fahrenheit);

bool QDUIRAWtoBool(QByteArray *Data, int addr, bool cut = false);
void QDUIBoolToRAW(QByteArray *Data, int addr, bool state);
quint8 QDUIRAWtoByte(QByteArray *Data, int addr, bool cut = false);
void QDUIByteToRAW(QByteArray *Data, int addr, quint8 value);
qint8 QDUIRAWtoInt8(QByteArray *Data, int addr, bool cut = false);
void QDUIInt8ToRAW(QByteArray *Data, int addr, qint8 value);
quint16 QDUIRAWtoUInt16(QByteArray *Data, int addr, bool cut = false);
void QDUIUInt16ToRAW(QByteArray *Data, int addr, quint16 value);
qint16 QDUIRAWtoInt16(QByteArray *Data, int addr, bool cut = false);
void QDUIInt16ToRAW(QByteArray *Data, int addr, qint16 value);
qint32 QDUIRAWtoInt32(QByteArray *Data, int addr, bool cut = false);
void QDUIInt32ToRAW(QByteArray *Data, int addr, qint32 value);
quint32 QDUIRAWtoUInt32(QByteArray *Data, int addr, bool cut = false);
void QDUIUInt32ToRAW(QByteArray *Data, int addr, quint32 value);
qreal QDUIRAWtoTemperature(QByteArray *Data, int addr, bool cut = false);
void QDUITemperatureToRAW(QByteArray *Data, int addr, qreal value);
QString QDUIRAWtoASCII(QByteArray *Data, int addr, bool cut = false);
void QDUIASCIIToRAW(QByteArray *Data, int addr, QString text);
QDate QDUIRAWtoDate(QByteArray *Data, int addr, bool cut = false);
void QDUIDateToRAW(QByteArray *Data, int addr, QDate date);
QTime QDUIRAWtoTime(QByteArray *Data, int addr, bool cut = false);
void QDUITimeToRAW(QByteArray *Data, int addr, QTime time);



// utils
QDir QDUIHomeDir();
QString QDUINormalizationFileName(QString name);


#endif // QDUISHAPES_H
