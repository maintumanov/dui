<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>About</name>
    <message>
        <location filename="about.ui" line="14"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="about.ui" line="117"/>
        <source>Close</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <location filename="about.cpp" line="17"/>
        <source>Author: Tumanov Stanislav Aleksandrovich</source>
        <translation>Автор: Туманов Станислав Александрович</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>DUI</source>
        <translation>ДИП</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="75"/>
        <source>Select port for connect:</source>
        <translation>Вибирете порт для подключения:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="147"/>
        <source>Connect</source>
        <translation>Подключиться</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="69"/>
        <source>Unable to connect!</source>
        <translation>Не удалось подключиться!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="140"/>
        <source>  manufacturer: </source>
        <translation>  производитель: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="146"/>
        <source>  description: </source>
        <translation>  описание: </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="qduishapes.cpp" line="14"/>
        <source>TRUE</source>
        <translation>ИСТИНА</translation>
    </message>
    <message>
        <location filename="qduishapes.cpp" line="15"/>
        <source>FALSE</source>
        <translation>ЛОЖЬ</translation>
    </message>
    <message>
        <location filename="qduishapes.cpp" line="17"/>
        <source>ON</source>
        <translation>ВКЛИЧЕНО</translation>
    </message>
    <message>
        <location filename="qduishapes.cpp" line="18"/>
        <source>OFF</source>
        <translation>ВЫКЛЮЧЕНО</translation>
    </message>
    <message>
        <location filename="qduishapes.cpp" line="20"/>
        <source>YES</source>
        <translation>ДА</translation>
    </message>
    <message>
        <location filename="qduishapes.cpp" line="21"/>
        <source>NO</source>
        <translation>НЕТ</translation>
    </message>
    <message>
        <location filename="qduishapes.cpp" line="23"/>
        <source>OPEN</source>
        <translation>ОТКРЫТО</translation>
    </message>
    <message>
        <location filename="qduishapes.cpp" line="24"/>
        <source>CLOSE</source>
        <translation>ЗАКРЫТО</translation>
    </message>
</context>
<context>
    <name>QSimpleTickerGraph</name>
    <message>
        <location filename="qsimpletickergraph.cpp" line="105"/>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
</context>
<context>
    <name>duiItemAbstract</name>
    <message>
        <location filename="duiitemabstract.cpp" line="8"/>
        <source>Channel %1</source>
        <translation>Канал %1</translation>
    </message>
</context>
<context>
    <name>duiItemLabel</name>
    <message>
        <location filename="duiitemlabel.cpp" line="17"/>
        <source>%1 %2</source>
        <translation></translation>
    </message>
</context>
</TS>
