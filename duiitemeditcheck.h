#ifndef DUIITEMEDITCHECK_H
#define DUIITEMEDITCHECK_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QFrame>
#include <QDebug>
#include <QCheckBox>
#include "duiitemabstract.h"
#include "qduishapes.h"

class duiItemEditCheck : public duiItemAbstract
{
    Q_OBJECT
public:
    explicit duiItemEditCheck(quint8 channel, QByteArray *data, QWidget *parent = 0);

signals:
    void volEditChange(bool vol);

public slots:
    void reciveData(QByteArray Data);

private slots:
    void volueChanged(bool vol);

private:
    QCheckBox *volueCheck;
    bool vol;

    void initialization(QByteArray *data);
};

#endif // DUIITEMEDITCHECK_H
