#include "qdui.h"


QDUI::QDUI(QObject *parent) :
    QObject(parent)
{
    NewMsg.AnalysisState = 0;
    serial = new QSerialPort(this);
    connect(serial, SIGNAL(readyRead()), this, SLOT(AnalisisCicle()));
}

void QDUI::AnalisisCicle()
{
    QByteArray data = serial->readAll();
    for (int i =0; i < data.count(); i ++)
        Analysis(data[i]);
}

void QDUI::openPort()
{
    if (serial->isOpen()) return;
    formRecived = false;
    serial->setBaudRate((int)115200);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    if (serial->open(QIODevice::ReadWrite)) {
        connectStateChange(true);
        SendFormInit();
         QTimer::singleShot(2000, this, SLOT(SendFormInit()));
        // info(2, QString(tr("Opening the port successfully")));
    }
}

void QDUI::closePort()
{
    if (!serial->isOpen()) return;
    SendDisconnect();
    serial->close();
    connectStateChange(false);
}

void QDUI::setPortName(QString name)
{
    serial->setPortName(name);
}

void QDUI::Analysis(quint8 data)
{
    if (data == 254)
    {
        formRecived = false;
        reciveReinit();
        SendFormInit();
        return;
    }

    if (data == 253)
    {
        NewMsg.AnalysisState = 1;
        return;
    }

    if (data == 255)  NewMsg.AnalysisState += 50;
    else
        if (NewMsg.AnalysisState >= 50)
        {
            NewMsg.AnalysisState -= 50;
            data+=128;
        }

    switch (NewMsg.AnalysisState)
    {
    case 1:
        NewMsg.Msg.inititem = (data == 255);
        if (NewMsg.Msg.inititem) NewMsg.AnalysisState = 2;
        else {
            NewMsg.Msg.channel = data;
            NewMsg.AnalysisState = 12;
        }
        break;
    case 2:
        NewMsg.Msg.channel = data;
        NewMsg.AnalysisState = 3;
        break;
    case 3:
        NewMsg.Msg.type = data;
        NewMsg.AnalysisState = 4;
        break;
    case 4:
        NewMsg.Msg.dataSize = data;
        NewMsg.Msg.data.clear();
        NewMsg.AnalysisState = 5;
        break;
    case 5:
        NewMsg.Msg.data.append(static_cast<char>(data));
        if (NewMsg.Msg.data.count() == NewMsg.Msg.dataSize) {
            NewMsg.AnalysisState = 0;
            formRecived = true;
            reciveInit(NewMsg.Msg.channel, static_cast<quint8>(NewMsg.Msg.type), NewMsg.Msg.data);
        }
        break;
    case 12:
        NewMsg.Msg.dataSize = data;
        NewMsg.Msg.data.clear();
        NewMsg.AnalysisState = 13;
        break;
    case 13:
        NewMsg.Msg.data.append(static_cast<char>(data));
        if (NewMsg.Msg.data.count() == NewMsg.Msg.dataSize) {
            NewMsg.AnalysisState = 0;
            reciveData(NewMsg.Msg.channel, NewMsg.Msg.data);
        }
        break;
    }
}


void QDUI::SaveSettings(QSettings *settings)
{
    settings->setValue(QLatin1String("UARTport"), serial->portName());
    settings->setValue(QLatin1String("UARTspeed"), serial->baudRate());
}

void QDUI::LoadSettings(QSettings *settings)
{
    serial->setPortName(settings->value(QLatin1String("UARTport"), serial->portName()).toString());
    serial->setBaudRate(settings->value(QLatin1String("UARTspeed"), serial->baudRate()).toInt());
}

void QDUI::addStart(QByteArray *sendbuffer)
{
    sendbuffer->append(static_cast<char>(253));
}

void QDUI::addByte(quint8 data, QByteArray *sendbuffer)
{
    if (data < 253) sendbuffer->append(static_cast<char>(data));
    else {
        sendbuffer->append(static_cast<char>(255));
        sendbuffer->append(static_cast<char>(data) - 128);
    }
}

void QDUI::addData(QByteArray *data, QByteArray *sendbuffer)
{
    if (data->count() == 0) return;
    addByte(static_cast<quint8>(data->count()), sendbuffer);
    for(quint16 i = 0; i < data->count(); i++)
        addByte(static_cast<quint8>(data->at(i)), sendbuffer);
       // qDebug() << data->toHex() << sendbuffer->toHex() << data->count();
}

void QDUI::addWord(quint16 data, QByteArray *sendbuffer)
{
    addByte((data >> 8) & 0xFF, sendbuffer);
    addByte(data & 0xFF, sendbuffer);
}

void QDUI::SendFormInit()
{
    if(formRecived) return;
    QByteArray sendbuffer;
    addStart(&sendbuffer);
    addByte(255, &sendbuffer);
    serial->write(sendbuffer);
    serial->waitForBytesWritten(10);
}

void QDUI::SendDisconnect()
{
    QByteArray sendbuffer;
    addStart(&sendbuffer);
    addByte(254, &sendbuffer);
    serial->write(sendbuffer);
    serial->waitForBytesWritten(10);
}

void QDUI::SendData(quint8 Channel, QByteArray Data)
{
    QByteArray sendbuffer;
    addStart(&sendbuffer);
    addByte(Channel, &sendbuffer);
    addData(&Data, &sendbuffer);
    serial->write(sendbuffer);
}

//void QDUI::SendInt8(quint8 Channel, quint8 type, quint8 vol)
//{
//    QByteArray sendbuffer;
//    addStart(&sendbuffer);
//    addByte(Channel, &sendbuffer);
//    addByte(1, &sendbuffer);
//    addByte(vol, &sendbuffer);
//    serial->write(sendbuffer);
//}

//void QDUI::SendInt16(quint8 Channel, quint8 type, quint16 vol)
//{
//    QByteArray sendbuffer;
//    addStart(&sendbuffer);
//    addByte(Channel, &sendbuffer);
//    addByte(2, &sendbuffer);
//    addWord(vol, &sendbuffer);
//    serial->write(sendbuffer);
//}




