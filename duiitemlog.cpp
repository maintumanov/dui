#include "duiitemlog.h"


duiItemLog::duiItemLog(quint8 channel, QByteArray *data, QWidget *parent) :
    duiItemAbstract(channel, parent)
{
    lines = 10;
    log = new QListWidget(this);
    log->setAlternatingRowColors(true);
    initialization(data);
    log->setMinimumHeight(lines * 5);
    setAutoFillBackground(true);
    QLabel *nameLabel = new QLabel(getName());
    nameLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);

    pbutton = new QPushButton();
    pbutton->setIconSize(QSize(16, 16));
    pbutton->setIcon(QIcon(QLatin1String(":/ActionIcons/ico/iClear.png")));
    connect(pbutton, SIGNAL(clicked()), this, SLOT(clear()));

    pbuttonp = new QPushButton();
    pbuttonp->setIconSize(QSize(16, 16));
    pbuttonp->setIcon(QIcon(QLatin1String(":/ActionIcons/ico/iPlus.png")));
    connect(pbuttonp, SIGNAL(clicked()), this, SLOT(plus()));
    pbuttonm = new QPushButton();
    pbuttonm->setIconSize(QSize(16, 16));
    pbuttonm->setIcon(QIcon(QLatin1String(":/ActionIcons/ico/iMinus.png")));
    connect(pbuttonm, SIGNAL(clicked()), this, SLOT(minus()));

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);
    mainLayout->addWidget(&frame);
    setLayout(mainLayout);
    QHBoxLayout *titleLayout = new QHBoxLayout;
    titleLayout->setMargin(2);
    titleLayout->setSpacing(2);
    titleLayout->addWidget(nameLabel);
    titleLayout->addSpacing(10);
    titleLayout->addWidget(pbuttonp);
    titleLayout->addWidget(pbuttonm);
    titleLayout->addWidget(pbutton);
    QVBoxLayout *valueLayout = new QVBoxLayout(&frame);
    valueLayout->setMargin(2);
    valueLayout->setSpacing(2);
    valueLayout->addLayout(titleLayout);
    valueLayout->addWidget(log);

}

duiItemLog::~duiItemLog()
{

}

void duiItemLog::reciveData(QByteArray Data)
{
    addLog(QDUITypeRAWtoString(&Data, QString()));
}

void duiItemLog::clear()
{
    log->clear();
}

void duiItemLog::initialization(QByteArray *data)
{
    QByteArray param = *data;
    setLines(QDUIRAWtoByte(&param, 0, true));
    setName(QDUIRAWtoASCII(&param, 0, true));
}

void duiItemLog::setLines(int l)
{
    lines = l;
}

void duiItemLog::addLog(QString text)
{
    log->insertItem(0, new QListWidgetItem(QString("%1   %2").arg(QDateTime::currentDateTime().toString("hh:mm:ss.zzz")).arg(text)));
    if (log->count() > 200) {
        QListWidgetItem *item = log->item(log->count() - 1);
        log->takeItem(log->count() - 1);
        delete item;
    }
}

void duiItemLog::plus()
{
    if (lines >= 300) return;
    lines += 5;
    log->setMinimumHeight(lines * 5);
}

void duiItemLog::minus()
{
    if (lines <= 10) return;
    lines -= 5;
    log->setMinimumHeight(lines * 5);
}

