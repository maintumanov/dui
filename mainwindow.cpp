#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    set_mode_gui(0);
    msgTimer = new QTimer(this);
    DUI = new QDUI(this);
    fillPortsInfo();

    connect(DUI, SIGNAL(connectStateChange(bool)), this, SLOT(stateConnectChange(bool)));
    connect(msgTimer, SIGNAL(timeout()), this, SLOT(showMessageTimeOut()));
    connect(ui->button_msg, SIGNAL(clicked()), this, SLOT(showMessageTimeOut()));
    connect(ui->button_connect, SIGNAL(clicked()), DUI, SLOT(openPort()));
    connect(ui->portsView, SIGNAL(currentRowChanged(int)), this, SLOT(changeSelectedPort(int)));
    connect(ui->portsView, SIGNAL(doubleClicked(QModelIndex)), DUI, SLOT(openPort()));
    connect(DUI, SIGNAL(reciveInit(quint8,quint8,QByteArray)), this, SLOT(reciveInit(quint8,quint8,QByteArray)));
    connect(DUI, SIGNAL(reciveData(quint8,QByteArray)), this, SLOT(reciveData(quint8,QByteArray)));
    connect(DUI, SIGNAL(reciveReinit()), this, SLOT(clearItems()));
    connect(ui->button_info_2, SIGNAL(clicked()), this, SLOT(on_button_info_clicked()));

    loadSettings();

    ui->frame_msg_label->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    DUI->closePort();
    event->accept();
}

void MainWindow::on_button_close_clicked()
{
    DUI->closePort();
}

void MainWindow::stateConnectChange(bool state)
{
    if (state)  set_mode_gui(3);
    else {
        set_mode_gui(0);
        clearItems();
    }
}

void MainWindow::set_mode_gui(int mode)
{
    programState = static_cast<quint8>(mode);
    if (mode < 2) ui->pages->setCurrentIndex(0);
    if (mode > 1) ui->pages->setCurrentIndex(1);

    ui->connectPanel->setVisible(mode < 2);
    ui->button_connect->setEnabled(mode != 1);
}

void MainWindow::error_connect()
{
    if (programState != 1) return;
    DUI->closePort();
    showMessage(0, QString(tr("Unable to connect!")));
}

void MainWindow::showMessage(int importance, QString msg)
{
    setFrameColor(importance, ui->frame_msg_label);
    ui->label_msg->setText(msg);
    ui->frame_msg_label->setVisible(true);
    msgTimer->start(8000);
}

void MainWindow::showMessageTimeOut()
{
    ui->frame_msg_label->setVisible(false);
    msgTimer->stop();
}

void MainWindow::setFrameColor(int importance, QFrame *frame)
{
    QPalette pal = frame->palette();
    switch (importance)
    {
    case 0:
        pal.setBrush(QPalette::Background, QBrush(QColor(254, 138, 145, 255), Qt::SolidPattern));
        break;
    case 1:
        pal.setBrush(QPalette::Background, QBrush(QColor(254, 241, 138, 255), Qt::SolidPattern));
        break;
    case 2:
        pal.setBrush(QPalette::Background, QBrush(QColor(172, 246, 176, 255), Qt::SolidPattern));
        break;
    }
    frame->setPalette(pal);
}

void MainWindow::loadSettings()
{
    QSettings sttings;
    ui->portsView->setCurrentRow(sttings.value("last_connect_num_port", 0).toInt());
}

void MainWindow::saveSettings()
{
    QSettings sttings;
    sttings.setValue("last_connect_num_port", ui->portsView->currentRow());
}

void MainWindow::fillPortsInfo()
{
    ui->portsView->clear();

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        QFont fnt;
        QListWidgetItem *item = new QListWidgetItem();
        QWidget *itemWidget = new QWidget();
        QVBoxLayout *itemlayout = new QVBoxLayout(itemWidget);
        itemlayout->setMargin(2);
        itemlayout->setSpacing(2);

        QLabel *labelName = new QLabel(info.portName());
        fnt = labelName->font();
        fnt.setPointSize(10);
        fnt.setBold(true);
        labelName->setFont(fnt);
        itemlayout->addWidget(labelName);

        fnt.setPointSize(8);
        fnt.setBold(false);
        fnt.setItalic(true);

        if (!info.manufacturer().isEmpty()) {
            labelName = new QLabel(tr("  manufacturer: ") + info.manufacturer());
            labelName->setFont(fnt);
            itemlayout->addWidget(labelName);
        }

        if (!info.description().isEmpty()) {
            labelName = new QLabel(tr("  description: ") + info.description());
            labelName->setFont(fnt);
            itemlayout->addWidget(labelName);
        }

        item->setData(1000, info.portName());
        item->setSizeHint(itemWidget->sizeHint());
        ui->portsView->addItem(item);
        ui->portsView->setItemWidget(item, itemWidget);
    }
}

void MainWindow::changeSelectedPort(int index)
{
    DUI->setPortName(ui->portsView->item(index)->data(1000).toString());
    ui->button_connect->setEnabled(index != -1);
}

void MainWindow::on_button_info_clicked()
{
    About ab(this);
    ab.exec();
}

void MainWindow::reciveData(quint8 Channel, QByteArray Data)
{
    for (int i = 0; i < items.count(); i ++)
        if (items.at(i)->getChannel() == Channel) items.at(i)->reciveData(Data);
}

void MainWindow::reciveInit(quint8 channel, quint8 type, QByteArray Data)
{
    duiItemAbstract *item;

    switch(type) {
    case 1:
        item = new duiItemLabel(channel, &Data);
        items.append(item);
        ui->areaLayout->insertWidget(ui->areaLayout->count() - 1, item);
        break;
    case 2:
        item = new duiItemLed(channel, &Data);
        items.append(item);
        ui->areaLayout->insertWidget(ui->areaLayout->count() - 1, item);

        break;
    case 3:
        item = new duiItemChart(channel, &Data);
        items.append(item);
        ui->areaLayout->insertWidget(ui->areaLayout->count() - 1, item);
        break;
    case 4:
        item = new duiItemEditInt(channel, &Data);
        items.append(item);
        ui->areaLayout->insertWidget(ui->areaLayout->count() - 1, item);
        connect(item, SIGNAL(sendData(quint8,QByteArray)), DUI, SLOT(SendData(quint8,QByteArray)));
        break;
    case 5:
        item = new duiItemEditSlider(channel, &Data);
        items.append(item);
        ui->areaLayout->insertWidget(ui->areaLayout->count() - 1, item);
        connect(item, SIGNAL(sendData(quint8,QByteArray)), DUI, SLOT(SendData(quint8,QByteArray)));
        break;
    case 6:
        item = new duiItemEditCheck(channel, &Data);
        items.append(item);
        ui->areaLayout->insertWidget(ui->areaLayout->count() - 1, item);
        connect(item, SIGNAL(sendData(quint8,QByteArray)), DUI, SLOT(SendData(quint8,QByteArray)));
        break;
    case 7:
        item = new duiItemEditString(channel, &Data);
        items.append(item);
        ui->areaLayout->insertWidget(ui->areaLayout->count() - 1, item);
        connect(item, SIGNAL(sendData(quint8,QByteArray)), DUI, SLOT(SendData(quint8,QByteArray)));
        break;
    case 8:
        item = new duiItemEditButton(channel, &Data);
        items.append(item);
        ui->areaLayout->insertWidget(ui->areaLayout->count() - 1, item);
        connect(item, SIGNAL(sendData(quint8,QByteArray)), DUI, SLOT(SendData(quint8,QByteArray)));
        break;
    case 9:
        item = new duiItemEditTemperature(channel, &Data);
        items.append(item);
        ui->areaLayout->insertWidget(ui->areaLayout->count() - 1, item);
        connect(item, SIGNAL(sendData(quint8,QByteArray)), DUI, SLOT(SendData(quint8,QByteArray)));
        break;
    case 10:
        item = new duiItemLog(channel, &Data);
        items.append(item);
        ui->areaLayout->insertWidget(ui->areaLayout->count() - 1, item);
        connect(item, SIGNAL(sendData(quint8,QByteArray)), DUI, SLOT(SendData(quint8,QByteArray)));
        break;
    }
}

void MainWindow::clearItems()
{
    while (!items.isEmpty()) {
        delete items.last();
        items.removeLast();
    }
}

